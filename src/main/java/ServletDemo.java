package com.sulei.demo;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeewx.api.report.datastatistics.useranalysis.JwUserAnalysisAPI;
import org.jeewx.api.wxbase.wxtoken.JwTokenAPI;
import org.jeewx.api.report.datastatistics.useranalysis.model.UserAnalysisRtnInfo;
import org.jeewx.api.core.exception.WexinReqException;
import org.jeewx.api.core.common.AccessToken;


/**
 * Servlet implementation class ServletDemo
 */
public class ServletDemo extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private String token = null;
    private AccessToken accessToken = null;

    public ServletDemo() {
        super();
        //JwTokenAPI jwTokenAPI = new JwTokenAPI();
        //JwUserAnalysisAPI api =new JwUserAnalysisAPI();
        //api.getUserSummary(this.token,"2017-08-01","2017-08-30");
       /* try {
            this.token = jwTokenAPI.getAccessToken("wx422953aaf4a73942", "81b782dfee94852c952023b9555422dd");
        }catch (Exception e){
                System.out.println(e.toString());
        }*/
        //jwTokenAPI.getAccessToken("wx422953aaf4a73942","81b782dfee94852c952023b9555422dd");
        //this.accessToken = new AccessToken("wx422953aaf4a73942", "81b782dfee94852c952023b9555422dd"));
    }

    public static void printToken(){

        AccessToken accessToken  = new AccessToken("wx422953aaf4a73942", "81b782dfee94852c952023b9555422dd");
        String tokenS  = accessToken.getNewAccessToken();
        String tokenT =null;
        //String id = accessToken.getAppid();
       // String requestUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET".replace("APPID", accessToken.getAppid()).replace("APPSECRET", accessToken.getAppscret());
       // accessToken.getNewAccessToken();
        try {
             tokenT = JwTokenAPI.getAccessToken("wx422953aaf4a73942", "81b782dfee94852c952023b9555422dd");

        }catch (WexinReqException e){
            tokenT = e.toString();
        }

        System.out.println(tokenS);
        System.out.println(tokenT);
       // System.out.println("this.token");

    }
    protected List<UserAnalysisRtnInfo>  doReport(){

        JwUserAnalysisAPI api =new JwUserAnalysisAPI();

        try{

        return api.getUserSummary(this.token,"2017-08-01","2017-08-30");

        }catch (Exception e){

        System.out.println(e.toString());

        }

        return null;
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");

        String action = request.getParameter("action");
        if("login_input".equals(action)) {
            request.getRequestDispatcher("login.jsp").forward(request , response);
        } else if("login".equals(action)) {
            String name = request.getParameter("name");
            String password = request.getParameter("password");

            System.out.println("name->" + name + ",password->" + password);
        }
    }

}